import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { selectUser } from '../../../actions/user/userSlice';


function Header() {
	const list = useSelector(selectUser);
	const dispatch = useDispatch();

	const [productIds, setProductIds] = useState([]);
	const [cartCount, setCartCount] = useState('');

	useEffect(() => {
		
		console.log("Setted values are ==", list.productIds);
		setProductIds(list.productIds);
		setCartCount(list.cartCount);
		
	},[])

	useEffect(() => {
		
		console.log("Setted values are ==", list.productIds);
		// setProductIds(list.productIds);
		setCartCount(list.cartCount);
		
	},[setCartCount, list.cartCount])

	const displayCart= async()=> {

		console.log("Cart count is ==", cartCount);

	}

	
	return (
		<>
			<header class="header_area">
				<nav class="navbar navbar-expand-lg">
					
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						
						<ul class="navbar-nav">
							<li class="nav-item">
								<a class="nav-link" href="JavaScript:Void(0);">Your Cart</a>
							</li>
							
							<li class="nav-item">
								<a href="javascript:void(0);" class="nav-link btn btn-head" onClick={displayCart}>{cartCount}</a>
							</li>
									
						</ul>
					</div>
				</nav>
			</header>
		</>
	)
}

export default Header;