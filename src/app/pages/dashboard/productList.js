import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Header from '../layout/header.js';
import { products } from '../../constants/global.js';

import { setData, selectUser } from '../../../actions/user/userSlice.js';


function ProductList() {
	const list = useSelector(selectUser);
	const dispatch = useDispatch();
	
	const [loading, setLoading] = useState(false);
	const [productList, setProductList] = useState([]);
	const [productIds, setProductIds] = useState([]);
	const [cartCount, setCartCount] = useState('');

	useEffect(() => {
		setProductList(products);
		console.log("already setted datas are ==", list);
		setProductIds(list.productIds);
		setCartCount(list.cartCount);
	}, []);

	useEffect(() => {
		console.log("already setted datas are ==", list);
		setProductIds(list.productIds);
		setCartCount(list.cartCount);
	}, [setProductIds, setCartCount, list]);

	//for adding to cart 
	const addToCart = async (index) => {
		console.log("product ids are ==", list);
		let selectedProduct = productList[index];
		let newIds = [...list.productIds, selectedProduct.id];
		let payload = {
			'productIds': newIds,
			'cartCount': newIds.length
		};

		console.log("payload is being sent is ==", payload);
		
		dispatch(setData(payload));
		
		
	}
	
	// For removing from added cart- 
	const removeCart= (index)=>{

		let selectedProduct = productList[index];
		let array = Object.values(productIds);
		let index2 = array.indexOf(selectedProduct.id);
		array.splice(index2, 1);

		let payload = {
			'productIds': array,
			'cartCount': array.length
		};

		dispatch(setData(payload));

	}



	
	return (
		<>
			<Header />
			<section class="my-classes">
				
				<div class="container-fluid">
					<div class="heading">
						<h2>Our Products</h2>
					</div>

					<div class="row">
						{products && products.length > 0 && products.map((data, index) => {
							return <div class="col-lg-4" key={index}>
								<div class="outer-sec row">
									<div class="figure">
										<img src={'../images/'+data.pic} alt="" />
									</div>

									<div class="figcaption">
										<h5>Name: {data.name}</h5>
										<strong>Price: {data.price}</strong>
										{data.id && productIds.includes(data.id)?
											<span class="d-block">Added</span>
										:null}
										<br/>
										{data.id && !productIds.includes(data.id)?
											<a href="JavaScript:Void(0);" onClick={() => addToCart(index)}>Add to cart</a>
										:null}
										
										{data.id && productIds.includes(data.id)?
											<a href="JavaScript:Void(0);" onClick={() => removeCart(index)}>Remove from cart</a>
										:null}
									</div>
								
								</div>
							</div>
						})}
						
					</div>
				</div>
			</section>
		</>
	)

}

export default ProductList;