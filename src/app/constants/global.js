
export const products = [
    {
        'id': '001',
        'name': 'Ball Pen',
        'price': '100',
        'pic': 'img1.png' 
    },
    {
        'id': '002',
        'name': 'Dot Pen',
        'price': '200',
        'pic': 'img2.jpeg'
    },
    {
        'id': '003',
        'name': 'Link Pen',
        'price': '300',
        'pic': 'img3.png'
    },
    {
        'id': '004',
        'name': 'Jel Pen',
        'price': '400',
        'pic': 'img4.png'
    }
]
