import React from 'react';
import {
  BrowserRouter,
  Route,
  Switch,
} from 'react-router-dom'

// Importing all pages after auth -- / inside APP - 

// Sanga Pages/ APP -- 
import Card from './app/pages/dashboard/card';



function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path='/cartdetails' name='Front Page' component={Card} />
        
      </Switch>
    </BrowserRouter>
  );
}

export default App;
